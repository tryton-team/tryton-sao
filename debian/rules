#!/usr/bin/make -f

#export DH_VERBOSE=1

# set DEBIAN_VERSION* variables
include /usr/share/dpkg/pkg-info.mk
MAJOR = $(shell echo $(DEB_VERSION) | cut -d'.' -f 1,2)
VERSION = $(shell echo $(DEB_VERSION) | cut -d'-' -f 1)
define BANNER
/*! tryton-sao-$(VERSION) | GPL-3
This file is part of Tryton.  The COPYRIGHT file at the top level of
this repository contains the full copyright notices and license terms. */
endef
export BANNER

# The sequence of the files matters, so we can not just use a wildcard
JSFILES = src/sao.js \
          src/rpc.js \
          src/pyson.js \
          src/session.js \
          src/common.js \
          src/model.js \
          src/tab.js \
          src/screen.js \
          src/view.js \
          src/view/form.js \
          src/view/tree.js \
          src/view/graph.js \
          src/view/calendar.js \
          src/view/list_form.js \
          src/action.js \
          src/window.js \
          src/wizard.js \
          src/board.js \
          src/bus.js \
          src/plugins.js \
          src/html_sanitizer.js

POFILES = $(shell find locale -name "*.po")

# Path for po2json changed after bullseye
DEBIAN_RELEASE = $(shell grep 'VERSION_CODENAME=' /etc/os-release | cut -d'=' -f2)
ifeq ($(DEBIAN_RELEASE),bullseye)
	PO2JSON = /usr/bin/node-po2json
else
	PO2JSON = /usr/share/nodejs/po2json/bin/po2json
endif

%:
	dh $@

override_dh_auto_clean:
	dh_clean
	# Remove evtl. preprocessed files
	rm -rf dist/*
	rm -rf locale/*.json
	rm -rf bower_components/bootstrap-rtl-ondemand/bootstrap

override_dh_gencontrol:
	dh_gencontrol -- -Vversion:major="$(MAJOR)"

override_dh_install:
	# node-grunt-contrib-less is not (yet) available, so we mimic the tasks
	
	# Grunt task concat: Pull all js files into one single file
	mkdir -p dist
	for FILE in $(JSFILES); do cat $$FILE >> dist/tryton-sao.js; done
	
	# Grunt task uglify: Minify the js file
	(echo $$BANNER; uglifyjs dist/tryton-sao.js) > dist/tryton-sao.min.js

	# Grunt task less: Compile and compress the css files
	ln -s ../bootstrap bower_components/bootstrap-rtl-ondemand/bootstrap
	# Use the linked in path for bootstrap in --include-path to avoid duplicates
	lessc --include-path="bower_components/bootstrap-rtl-ondemand/bootstrap/less:bower_components/bootstrap-rtl-ondemand/less" src/sao.less dist/tryton-sao.css
	# minify with clean-css instead of deprecated lessc --compress
	cleancss -o dist/tryton-sao.min.css dist/tryton-sao.css

	# Grunt task po2json: Convert po files to json format
	for POFILE in $(POFILES); do \
		if [ -e $$POFILE ]; then \
			$(PO2JSON) --format raw $$POFILE `echo $$POFILE | cut -d'.' -f1`.json; \
		fi; \
	done

	# Image license is covered in copyright, no need to install it
	dh_install \
		-Xbower.json \
		-XGruntfile.js \
		-Xpackage.json \
		-Ximages/LICENSE

get-orig-source:
	./debian/get_orig_source.sh
