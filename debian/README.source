Source for tryton-sao
=====================

This package uses tarballs built from a fork of 
    https://github.com/tryton/tryton
hosted at
    https://salsa.debian.org/tryton-team/tryton-sao-debian

We must provide the vendored bundle because we currently do not have
the possibility to maintain multiple versioned JS libs for different
Debian releases (backports).

The automatic creation of tarballs is documented in get_orig_source.sh.

The resulting tarballs are hosted at people.debian.org:/home/mbehrle/public_html/downloads/tryton/$major


tryton-sao-debian
-----------------

The differences in the tarballs created are

  - Include vendored Javascript libraries

  - Omit json files (they are (re-)built from source on the Debian build)

  - Omit dist files (they are (re-)built from source on the Debian build)


Procedure
---------

The Debian specific tarballs are created from a Debian specific branch called <major>-debian.

Once a new release has been published by the Tryton project the debian specific must be updated
to match the new Tryton release.

For example an update of the Tryton series 6.0:

    $ git clone git@salsa.debian.org:tryton-team/tryton-sao-debian.git
    $ git remote add gh https://github.com/tryton/tryton
    $ git fetch --all
    $ git checkout 6.0-debian
    $ git merge <tag_sao-version>

Note: Only merge up to the new version bump to not get the version of the next release!

    $ git push

Thereafter a new tarball can be created by running

    $ debian/get_orig_source.sh
