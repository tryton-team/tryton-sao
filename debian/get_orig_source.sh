#!/bin/bash
# This script documents the steps to get a vendored tarball for tryton-sao

## Requires apt-get install npm
version=$(npm version --json | python -c "import json,sys; print(json.load(sys.stdin)['tryton-sao'])")
major=$(echo $version | cut -d'.' -f 1,2)
projectname=tryton-sao-debian

# Use the Debian custom source repo to build tryton-sao
git clone https://salsa.debian.org/tryton-team/$projectname.git -b $major-debian
cd $projectname/sao

# Evtl. merge first the target release from $major
# Take care to only merge just before the new version bump to not precede the upstream versions
# with the debian versions

# Now get the definite version
version=$(npm version --json | python -c "import json,sys; print(json.load(sys.stdin)['tryton-sao'])")
filename=$projectname-$version.tgz

# Download the JS libs
npx --omit-dev bower install -y

# Pack the whole package including bower components
#quilt push -a
npm pack
mv tryton-sao-*.tgz $filename

# Pack only bower components
#tar -cJ bower_components/ -f $filename
# Pack as gz to avoid force repack to gz by uscan
#tar -cz bower_components/ -f $filename

# TODO
# Build and release automatically via Tags on salsa? Possible with CE?
#
# Sign and upload
gpg --detach-sign --armor $filename
scp $filename* people.debian.org:/home/mbehrle/public_html/downloads/tryton/$major/

# Remove build artifacts
rm $filename*
#cd ..
#rm -rf $projectname
